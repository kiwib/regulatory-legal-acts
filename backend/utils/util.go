package utils

import "os"

func GetEnvOrDefault(key string, defaultValue string) string {
	envValue := os.Getenv(key)
	if len(envValue) == 0 {
		return defaultValue
	}
	return envValue
}
