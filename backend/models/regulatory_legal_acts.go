package models

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"log"
)

type RegularLegalAct struct {
	Id                   uuid.UUID `json:"id";sql:"id"`
	Name                 string    `json:"name";sql:"name"`
	ComplexName          string    `json:"complex_name";sql:"complex_name"`
	DocumentDate         string    `json:"document_date";sql:"document_date"` // TODO: возможно придется поменять тип
	DocumentTypeId       uuid.UUID `json:"document_type_id";sql:"document_type_id"`
	EoNumber             string    `json:"eo_number";sql:"eo_number"`
	HasPdf               bool      `json:"has_pdf";sql:"has_pdf"`
	Number               string    `json:"number";sql:"number"`
	PdfFileLength        int64     `json:"pdf_file_length";sql:"pdf_file_length"`
	PublishDateShort     string    `json:"publish_date_short";sql:"publish_date_short"` // TODO: возможно придется поменять тип
	SignatoryAuthorityId uuid.UUID `json:"signatory_authority_id";sql:"signatory_authority_id"`
}

func InitializeRegulatoryLegalActsIntoDb(db *sqlx.DB, regulatoryLegalActs []domain.RegularLegalActDto) (bool, error) {
	/* Метод для инициализации НПА в БД */
	processedCounter := 0
	for _, regulatoryLegalAct := range regulatoryLegalActs {
		_, err := UpdateOrCreateRegulatoryLegalActIntoDb(db, regulatoryLegalAct)
		if err != nil {
			log.Println("Not processed object: ", regulatoryLegalAct)
			continue
		}
		processedCounter += 1
	}
	log.Println(fmt.Sprintf("Was proccessed %d records", processedCounter))
	return false, nil
}

func UpdateOrCreateRegulatoryLegalActIntoDb(db *sqlx.DB, regulatoryLegalAct domain.RegularLegalActDto) (bool, error) {
	/* Метод для создания или обновления вида документа */
	regulatoryLegalActIntoDb, err := GetRegulatoryLegalActInDbByID(db, regulatoryLegalAct)
	// если объекта нет в БД, то создаем
	if err != nil {
		_, err := CreateRegulatoryLegalActIntoDb(db, regulatoryLegalAct)
		if err != nil {
			log.Println("Error when object creating: ", err)
			return false, err
		}
		return true, nil
	}
	_, err = UpdateRegulatoryLegalActIntoDb(db, regulatoryLegalActIntoDb.Id.String(), regulatoryLegalAct)
	if err != nil {
		log.Println("Error when object updating: ", err)
		return false, err
	}
	return true, nil
}

func UpdateRegulatoryLegalActIntoDb(db *sqlx.DB, id string, regulatoryLegalAct domain.RegularLegalActDto) (bool, error) {
	/* Метод для обновления записи в БД о НПА */
	if _, err := UpdateOrCreateDocumentTypeIntoDb(db, domain.DocumentTypeDto{Id: regulatoryLegalAct.DocumentTypeId, Name: regulatoryLegalAct.DocumentTypeName}); err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | UpdateOrCreateDocumentTypeIntoDb: ", err)
	}
	documentType, err := GetDocumentTypeInDbByID(db, domain.DocumentTypeDto{Id: regulatoryLegalAct.DocumentTypeId, Name: regulatoryLegalAct.DocumentTypeName})
	if err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | GetDocumentTypeInDbByID: ", err)
	}
	if _, err := UpdateOrCreateSignatoryAuthorityIntoDb(db, domain.SignatoryAuthorityDto{Id: regulatoryLegalAct.SignatoryAuthorityId, Name: regulatoryLegalAct.SignatoryAuthorityName}); err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | UpdateOrCreateSignatoryAuthorityIntoDb: ", err)
	}
	regulatoryAuthority, err := GetSignatoryAuthorityInDbByID(db, domain.SignatoryAuthorityDto{Id: regulatoryLegalAct.SignatoryAuthorityId, Name: regulatoryLegalAct.SignatoryAuthorityName})
	if err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | GetSignatoryAuthorityInDbByID: ", err)
	}
	query := fmt.Sprintf("UPDATE regulatory_legal_acts SET id = '%s', eo_number = '%s', name = '%s', complex_name = '%s', has_pdf = %t, pdf_file_length = %d, publish_date_short = '%s', document_date = '%s', document_type_id = '%s', signatory_authority_id = '%s' WHERE id = '%s'", regulatoryLegalAct.Id, regulatoryLegalAct.EoNumber, regulatoryLegalAct.Name, regulatoryLegalAct.ComplexName, regulatoryLegalAct.HasPdf, regulatoryLegalAct.PdfFileLength, regulatoryLegalAct.PublishDateShort, regulatoryLegalAct.DocumentDate, documentType.Id.String(), regulatoryAuthority.Id.String(), id)
	if _, err := db.Exec(query); err != nil {
		return false, err
	}
	return true, nil
}

func CreateRegulatoryLegalActIntoDb(db *sqlx.DB, regulatoryLegalAct domain.RegularLegalActDto) (bool, error) {
	/* Метод для создания записи в БД о НПА */
	if _, err := UpdateOrCreateDocumentTypeIntoDb(db, domain.DocumentTypeDto{Id: regulatoryLegalAct.DocumentTypeId, Name: regulatoryLegalAct.DocumentTypeName}); err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | UpdateOrCreateDocumentTypeIntoDb: ", err)
	}
	documentType, err := GetDocumentTypeInDbByID(db, domain.DocumentTypeDto{Id: regulatoryLegalAct.DocumentTypeId, Name: regulatoryLegalAct.DocumentTypeName})
	if err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | GetDocumentTypeInDbByID: ", err)
	}
	if _, err := UpdateOrCreateSignatoryAuthorityIntoDb(db, domain.SignatoryAuthorityDto{Id: regulatoryLegalAct.SignatoryAuthorityId, Name: regulatoryLegalAct.SignatoryAuthorityName}); err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | UpdateOrCreateSignatoryAuthorityIntoDb: ", err)
	}
	regulatoryAuthority, err := GetSignatoryAuthorityInDbByID(db, domain.SignatoryAuthorityDto{Id: regulatoryLegalAct.SignatoryAuthorityId, Name: regulatoryLegalAct.SignatoryAuthorityName})
	if err != nil {
		log.Fatalln("CreateRegulatoryLegalActIntoDb | GetSignatoryAuthorityInDbByID: ", err)
	}
	query := fmt.Sprintf("INSERT INTO regulatory_legal_acts (id, eo_number, name, complex_name, has_pdf, pdf_file_length, publish_date_short, document_date, document_type_id, signatory_authority_id) VALUES('%s', '%s', '%s', '%s', %t, %d, '%s', '%s', '%s', '%s')", regulatoryLegalAct.Id, regulatoryLegalAct.EoNumber, regulatoryLegalAct.Name, regulatoryLegalAct.ComplexName, regulatoryLegalAct.HasPdf, regulatoryLegalAct.PdfFileLength, regulatoryLegalAct.PublishDateShort, regulatoryLegalAct.DocumentDate, documentType.Id.String(), regulatoryAuthority.Id.String())
	if _, err := db.Exec(query); err != nil {
		return false, err
	}
	return true, nil
}

func GetRegulatoryLegalActInDbByID(db *sqlx.DB, regulatoryLegalAct domain.RegularLegalActDto) (RegularLegalAct, error) {
	/* Функция для проверки наличие НПА в БД по идентификатору */
	var row RegularLegalAct
	query := fmt.Sprintf("SELECT id, eo_number as eonumber, name, complex_name as complexname, has_pdf as haspdf, pdf_file_length as pdffilelength, publish_date_short as publishdateshort, document_date as documentdate, document_type_id as documenttypeid, signatory_authority_id as signatoryauthorityid FROM regulatory_legal_acts WHERE id = '%s'", regulatoryLegalAct.Id)
	if err := db.Get(&row, query); err != nil {
		return RegularLegalAct{}, err
	}
	return row, nil
}

func GetRegulatoryLegalActsFromDb(db *sqlx.DB) ([]RegularLegalAct, error) {
	/* Функция для получения ВСЕХ НПА из БД */
	var rows []RegularLegalAct
	if err := db.Select(&rows, "SELECT id, eo_number as eonumber, name, complex_name as complexname, has_pdf as haspdf, pdf_file_length as pdffilelength, publish_date_short as publishdateshort, document_date as documentdate, document_type_id as documenttypeid, signatory_authority_id as signatoryauthorityid FROM regulatory_legal_acts"); err != nil {
		return nil, err
	}
	if rows == nil {
		return []RegularLegalAct{}, nil
	}
	return rows, nil
}
