package models

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"log"
)

type SignatoryAuthority struct {
	Id   uuid.UUID `json:"id";sql:"id"`
	Name string    `json:"name";sql:"name"`
}

func InitializeSignatoryAuthorityIntoDb(db *sqlx.DB, signatoryAuthoritiesDto []domain.SignatoryAuthorityDto) (bool, error) {
	/* Метод для инициализации принимающих органов в БД */
	processedCounter := 0
	for _, signatoryAuthority := range signatoryAuthoritiesDto {
		_, err := UpdateOrCreateSignatoryAuthorityIntoDb(db, signatoryAuthority)
		if err != nil {
			log.Println("Not processed object: ", signatoryAuthority)
			continue
		}
		processedCounter += 1
	}
	log.Println(fmt.Sprintf("Was proccessed %d records", processedCounter))
	return false, nil
}

func CreateSignatoryAuthorityIntoDb(db *sqlx.DB, signatoryAuthority domain.SignatoryAuthorityDto) (bool, error) {
	/* Метод для создания записи в БД о принимающем органе */
	query := fmt.Sprintf("INSERT INTO signatory_authority (id, name) VALUES('%s', '%s')", signatoryAuthority.Id, signatoryAuthority.Name)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateOrCreateSignatoryAuthorityIntoDb(db *sqlx.DB, signatoryAuthority domain.SignatoryAuthorityDto) (bool, error) {
	/* Метод для создания или обновления вида документа */
	documentTypeIntoDb, err := GetSignatoryAuthorityInDbByID(db, signatoryAuthority)
	// если объекта нет в БД, то создаем
	if err != nil {
		_, err := CreateSignatoryAuthorityIntoDb(db, signatoryAuthority)
		if err != nil {
			log.Println("Error when object creating: ", err)
			return false, err
		}
		return true, nil
	}
	_, err = UpdateSignatoryAuthorityIntoDb(db, documentTypeIntoDb.Id.String(), signatoryAuthority)
	if err != nil {
		log.Println("Error when object updating: ", err)
		return false, err
	}
	return true, nil
}

func UpdateSignatoryAuthorityIntoDb(db *sqlx.DB, id string, signatoryAuthority domain.SignatoryAuthorityDto) (bool, error) {
	/* Метод для обновления принимающего органа в БД */
	if _, err := GetSignatoryAuthorityInDbByID(db, signatoryAuthority); err != nil {
		return false, err
	}
	query := fmt.Sprintf("UPDATE signatory_authority SET id = '%s', name = '%s' WHERE id = '%s'", id, signatoryAuthority.Name, signatoryAuthority.Id)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetSignatoryAuthorityInDbByID(db *sqlx.DB, signatoryAuthority domain.SignatoryAuthorityDto) (SignatoryAuthority, error) {
	/* Функция для проверки наличие принимающего органа в БД по идентификатору */
	var row SignatoryAuthority
	query := fmt.Sprintf("SELECT * FROM signatory_authority WHERE id = '%s'", signatoryAuthority.Id)
	err := db.Get(&row, query)
	if err != nil {
		return SignatoryAuthority{}, err
	}
	return row, nil
}

func GetSignatoryAuthoritiesFromDb(db *sqlx.DB) ([]SignatoryAuthority, error) {
	/* Функция для получения ВСЕХ принимающих органов из БД */
	var rows []SignatoryAuthority
	err := db.Select(&rows, "SELECT id, name from signatory_authority")
	if err != nil {
		return nil, err
	}
	if rows == nil {
		return []SignatoryAuthority{}, nil
	}
	return rows, nil
}
