package models

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"log"
)

type Section struct {
	Code                         string `json:"code";sql:"code"`
	Name                         string `json:"name";sql:"name"`
	Description                  string `json:"description";sql:"description"`
	ParentCode                   string `json:"parent_code";sql:"parent_code"`
	IsAgenciesOfStateAuthorities bool   `json:"is_agencies_of_state_authorities";sql:"is_agencies_of_state_authorities"`
}

func InitializeSectionsIntoDb(db *sqlx.DB, sections []domain.SectionDto) (bool, error) {
	/* Метод для инициализации блоков в БД */
	processedCounter := 0
	for _, section := range sections {
		_, err := UpdateOrCreateSectionIntoDb(db, section)
		if err != nil {
			log.Println("Not processed object: ", section)
			continue
		}
		processedCounter += 1
	}
	log.Println(fmt.Sprintf("Was proccessed %d records", processedCounter))
	return false, nil
}

func CreateSectionIntoDb(db *sqlx.DB, section domain.SectionDto) (bool, error) {
	/* Метод для создания записи в БД о блоке (разделе) */
	query := fmt.Sprintf("INSERT INTO sections (code, description, name, is_agencies_of_state_authorities, parent_code) VALUES('%s', '%s', '%s', %t, '%s')", section.Code, section.Description, section.Name, section.IsAgenciesOfStateAuthorities, section.ParentCode)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateSectionIntoDb(db *sqlx.DB, code string, section domain.SectionDto) (bool, error) {
	/* Метод для обновления блока (раздела) в БД */
	if _, err := GetSectionInDbById(db, section); err != nil {
		return false, err
	}
	query := fmt.Sprintf("UPDATE sections SET code = '%s', description = '%s', name = '%s', is_agencies_of_state_authorities=%t, parent_code='%s' WHERE code = '%s'", section.Code, section.Description, section.Name, section.IsAgenciesOfStateAuthorities, section.ParentCode, code)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateOrCreateSectionIntoDb(db *sqlx.DB, section domain.SectionDto) (bool, error) {
	/* Метод для создания или обновления блока (раздела) */
	documentTypeIntoDb, err := GetSectionInDbById(db, section)
	// если объекта нет в БД, то создаем
	if err != nil {
		_, err := CreateSectionIntoDb(db, section)
		if err != nil {
			log.Println("Error when object creating: ", err)
			return false, err
		}
		return true, nil
	}
	_, err = UpdateSectionIntoDb(db, documentTypeIntoDb.Code, section)
	if err != nil {
		log.Println("Error when object updating: ", err)
		return false, err
	}
	return true, nil
}

func GetSubSectionsInDbByParentCode(db *sqlx.DB, section Section) ([]Section, error) {
	/* Функция для получения ВСЕХ блоков (разделов) НПА из бд */
	var rows []Section
	query := fmt.Sprintf("SELECT code, description, name, is_agencies_of_state_authorities as isagenciesofstateauthorities, parent_code as parentcode FROM sections WHERE parent_code = '%s'", section.Code)
	err := db.Select(&rows, query)
	if err != nil {
		log.Println("GetSubSectionsInDbByParentCode: ", err)
		return nil, err
	}
	if rows == nil {
		return []Section{}, nil
	}
	return rows, nil
}

func GetSectionInDbById(db *sqlx.DB, section domain.SectionDto) (Section, error) {
	/* Функция для проверки наличие вида документа в БД по идентификатору */
	var row = Section{}
	query := fmt.Sprintf("SELECT code, description, name, is_agencies_of_state_authorities as isagenciesofstateauthorities, parent_code as parentcode FROM sections WHERE code = '%s'", section.Code)
	err := db.Get(&row, query)
	if err != nil {
		return Section{}, err
	}
	return row, nil
}

func GetSectionsFromDb(db *sqlx.DB) ([]Section, error) {
	/* Функция для получения ВСЕХ блоков (разделов) НПА из бд */
	var rows []Section
	err := db.Select(&rows, "SELECT code, description, name, is_agencies_of_state_authorities as isagenciesofstateauthorities, parent_code as parentcode FROM sections WHERE parent_code = ''")
	if err != nil {
		log.Println("GetSectionsFromDb: ", err)
		return nil, err
	}
	if rows == nil {
		return []Section{}, nil
	}
	return rows, nil
}
