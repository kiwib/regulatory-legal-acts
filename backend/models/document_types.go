package models

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"log"
)

type DocumentType struct {
	Id   uuid.UUID `json:"id";sql:"id"`
	Name string    `json:"name";sql:"name"`
}

type DocumentTypeResponse struct {
	Paginator domain.Paginator `json:"paginator";`
	Data      []DocumentType   `json:"data";`
}

func InitializeDocumentTypesIntoDb(db *sqlx.DB, documentTypes []domain.DocumentTypeDto) (bool, error) {
	/* Метод для инициализации видов документа в БД */
	processedCounter := 0
	for _, documentType := range documentTypes {
		_, err := UpdateOrCreateDocumentTypeIntoDb(db, documentType)
		if err != nil {
			log.Println("Not processed object: ", documentType)
			continue
		}
		processedCounter += 1
	}
	log.Println(fmt.Sprintf("Was proccessed %d records", processedCounter))
	return false, nil
}

func CreateDocumentTypeIntoDb(db *sqlx.DB, documentType domain.DocumentTypeDto) (bool, error) {
	/* Метод для создания записи в БД о виде документа */
	query := fmt.Sprintf("INSERT INTO document_types (id, name) VALUES('%s', '%s')", documentType.Id, documentType.Name)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateDocumentTypeIntoDb(db *sqlx.DB, id string, documentType domain.DocumentTypeDto) (bool, error) {
	/* Метод для обновления вида документа в БД */
	if _, err := GetDocumentTypeInDbByID(db, documentType); err != nil {
		return false, err
	}
	query := fmt.Sprintf("UPDATE document_types SET id = '%s', name = '%s' WHERE id = '%s'", id, documentType.Name, documentType.Id)
	_, err := db.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateOrCreateDocumentTypeIntoDb(db *sqlx.DB, documentType domain.DocumentTypeDto) (bool, error) {
	/* Метод для создания или обновления вида документа */
	documentTypeIntoDb, err := GetDocumentTypeInDbByID(db, documentType)
	// если объекта нет в БД, то создаем
	if err != nil {
		_, err := CreateDocumentTypeIntoDb(db, documentType)
		if err != nil {
			log.Println("Error when object creating: ", err)
			return false, err
		}
		return true, nil
	}
	_, err = UpdateDocumentTypeIntoDb(db, documentTypeIntoDb.Id.String(), documentType)
	if err != nil {
		log.Println("Error when object updating: ", err)
		return false, err
	}
	return true, nil
}

func GetDocumentTypeInDbByID(db *sqlx.DB, documentType domain.DocumentTypeDto) (DocumentType, error) {
	/* Функция для проверки наличие вида документа в БД по идентификатору */
	var row DocumentType
	query := fmt.Sprintf("SELECT id, name FROM document_types WHERE id = '%s'", documentType.Id)
	err := db.Get(&row, query)
	if err != nil {
		return DocumentType{}, err
	}
	return row, nil
}

func GetDocumentTypesCountFromDb(db *sqlx.DB) int {
	var count int
	db.Get(&count, "SELECT COUNT(id) FROM  document_types")
	return count
}

func GetPaginateDocumentTypesFromDb(db *sqlx.DB, paginator domain.DbPaginator) ([]DocumentType, error) {
	/* Функция для получения ВСЕХ видов документов из БД */
	var rows []DocumentType
	query := fmt.Sprintf("SELECT id, name from document_types ORDER BY id DESC LIMIT %d OFFSET %d", paginator.Limit, paginator.Offset)
	err := db.Select(&rows, query)
	if err != nil {
		return nil, err
	}
	if rows == nil {
		return []DocumentType{}, nil
	}
	return rows, nil
}
