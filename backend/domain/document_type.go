package domain

import "github.com/google/uuid"

type DocumentTypeDto struct {
	Id   uuid.UUID `json:"Id"`
	Name string    `json:"Name"`
}
