package domain

import "github.com/google/uuid"

type SignatoryAuthorityDto struct {
	Id   uuid.UUID `json:"Id"`
	Name string    `json:"Name"`
}
