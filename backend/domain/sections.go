package domain

type SectionDto struct {
	Code                         string `json:"Code"`
	Name                         string `json:"Name"`
	ParentCode                   string `json:"ParentCode"`
	Description                  string `json:"Description"`
	IsAgenciesOfStateAuthorities bool   `json:"IsAgenciesOfStateAuthorities"`
}
