package domain

type Paginator struct {
	TotalRecord int32 `json:"total_record";`
	CurrentPage int32 `json:"current_page";`
	NextPage    int32 `json:"next_page";`
	TotalPage   int32 `json:"total_page";`
	PageSize    int32 `json:"page_size";`
}

type DbPaginator struct {
	Limit  int `json:"limit";`
	Offset int `json:"offset";`
}
