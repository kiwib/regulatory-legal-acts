package domain

import "github.com/google/uuid"

type RegularLegalActDto struct {
	Id                     uuid.UUID `json:"Id"`
	Name                   string    `json:"Name"`
	ComplexName            string    `json:"ComplexName"`
	DocumentDate           string    `json:"DocumentDate"` // TODO: возможно придется поменять тип
	DocumentTypeId         uuid.UUID `json:"DocumentTypeId"`
	DocumentTypeName       string    `json:"DocumentTypeName"`
	EoNumber               string    `json:"EoNumber"`
	HasPdf                 bool      `json:"HasPdf"`
	Number                 string    `json:"Number"`
	PdfFileLength          int64     `json:"PdfFileLength"`
	PublishDateShort       string    `json:"PublishDateShort"` // TODO: возможно придется поменять тип
	SignatoryAuthorityId   uuid.UUID `json:"SignatoryAuthorityId"`
	SignatoryAuthorityName string    `json:"SignatoryAuthorityName"`
}

type RegularLegalActDocumentDto struct {
	CurrentPageNumber         int32                `json:"CurrentPageNumber"`
	Documents                 []RegularLegalActDto `json:"Documents"`
	IsShowAppendPageCountList bool                 `json:"IsShowAppendPageCountList"`
	MaxPageNumber             int32                `json:"MaxPageNumber"`
	PageSize                  int32                `json:"PageSize"`
	TotalDocumentsCount       int32                `json:"TotalDocumentsCount"`
}
