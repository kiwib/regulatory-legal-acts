#!/bin/sh
set -e

cat /dev/null >.env

# shellcheck disable=SC2129
echo "DATABASE_PORT=$DATABASE_PORT" >>.env
echo "DATABASE_HOST=$DATABASE_HOST" >>.env
echo "DATABASE_PASSWORD=$DATABASE_PASSWORD" >>.env
echo "DATABASE_NAME=$DATABASE_NAME" >>.env
echo "DATABASE_USERNAME=$DATABASE_USERNAME" >>.env
