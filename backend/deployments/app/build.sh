#!/bin/sh
set -e

make build-app-alpine

make build-initialize-regulatory-legal-acts

make build-initialize-sections

make build-initialize-subsections

make build-initialize-document-types
