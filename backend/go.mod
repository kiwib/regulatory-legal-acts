module gitlab.com/kiwib/regulatory-legal-acts

go 1.17

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/lib/pq v1.10.4 // indirect
)
