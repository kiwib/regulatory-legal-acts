CREATE TABLE signatory_authority
(
    id   uuid         not null unique,
    name varchar(512) not null,
    PRIMARY KEY (id)
);
