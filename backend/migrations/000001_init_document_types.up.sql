CREATE TABLE document_types
(
    id   uuid         not null unique,
    name varchar(512) not null,
    PRIMARY KEY (id)
);
