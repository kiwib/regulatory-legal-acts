CREATE TABLE regulatory_legal_acts
(
--  Идентификационный номер документа (GUID)
    id                     uuid         not null unique,
--  Номер ЭО НПА
    eo_number              varchar(512) not null,
    name                   text         null,
--  Составное название НПА
    complex_name           text         null,
--  Признак наличия PDF файла
    has_pdf                bool              DEFAULT false,
--  размер PDF файла НПА
    pdf_file_length        integer           DEFAULT 0,
--  время опубликования НПА
    publish_date_short     timestamptz  not null,
    document_date          timestamptz  not null,

    document_type_id       uuid         null DEFAULT null REFERENCES document_types (id) ON DELETE SET DEFAULT,
    signatory_authority_id uuid         null DEFAULT null REFERENCES signatory_authority (id) ON DELETE SET DEFAULT,
    PRIMARY KEY (id)
);
