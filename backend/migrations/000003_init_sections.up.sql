CREATE TABLE sections
(
    code                             varchar(512)  not null unique,
    description                      varchar(1024) not null,
    name                             varchar(512)  not null,
    is_agencies_of_state_authorities bool                   DEFAULT false,
    parent_code                      CHARACTER VARYING(512) DEFAULT '',
    PRIMARY KEY (code)
);
