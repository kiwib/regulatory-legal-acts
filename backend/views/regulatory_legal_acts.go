package views

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
	"net/http"
)

func RegulatoryLegalActListView(response http.ResponseWriter, request *http.Request) {
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	regulatoryLegalActs, err := models.GetRegulatoryLegalActsFromDb(db)
	if err != nil {
		log.Println("RegulatoryLegalActListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	regulatoryLegalActsJson, _ := json.Marshal(regulatoryLegalActs)
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	response.Write(regulatoryLegalActsJson)
}

func RegulatoryLegalActDetailView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	idAsUuid, err := uuid.Parse(vars["id"])
	if err != nil {
		log.Println("RegulatoryLegalActDetailView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	regulatoryLegalAct, err := models.GetRegulatoryLegalActInDbByID(db, domain.RegularLegalActDto{Id: idAsUuid})
	regulatoryLegalActJson, _ := json.Marshal(regulatoryLegalAct)
	response.WriteHeader(http.StatusOK)
	response.Write(regulatoryLegalActJson)
}
