package views

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
	"net/http"
)

func SectionListView(response http.ResponseWriter, request *http.Request) {
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	sections, err := models.GetSectionsFromDb(db)
	if err != nil {
		log.Println("SectionListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	sectionsJson, _ := json.Marshal(sections)
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	response.Write(sectionsJson)
}

func SectionDetailView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	section, err := models.GetSectionInDbById(db, domain.SectionDto{Code: vars["code"]})
	if err != nil {
		log.Println("SectionDetailView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	sectionJson, _ := json.Marshal(section)
	response.WriteHeader(http.StatusOK)
	response.Write(sectionJson)
}

func SectionDetailSubSectionListView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	section, err := models.GetSectionInDbById(db, domain.SectionDto{Code: vars["code"]})
	if err != nil {
		log.Println("SectionDetailSubsectionListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	subSections, err := models.GetSubSectionsInDbByParentCode(db, section)
	if err != nil {
		log.Println("SectionDetailSubsectionListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	subSectionsJson, _ := json.Marshal(subSections)
	response.WriteHeader(http.StatusOK)
	response.Write(subSectionsJson)
}

func SectionDetailSubSectionDetailView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	section, err := models.GetSectionInDbById(db, domain.SectionDto{Code: vars["subCode"]})
	if err != nil {
		log.Println("SectionDetailSubsectionListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	subSectionJson, _ := json.Marshal(section)
	response.WriteHeader(http.StatusOK)
	response.Write(subSectionJson)
}
