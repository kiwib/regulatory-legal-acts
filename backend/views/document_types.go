package views

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
	"math"
	"net/http"
	"strconv"
)

func DocumentTypeListView(response http.ResponseWriter, request *http.Request) {
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	query := request.URL.Query()
	if !query.Has("page") {
		query.Set("page", "1")
	}
	if !query.Has("limit") {
		query.Set("limit", "10")
	}

	page, err := strconv.ParseInt(query.Get("page"), 10, 64)
	if err != nil {
		log.Println("DocumentTypeListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if err != nil {
		log.Println("DocumentTypeListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	// определяем общее количество записей
	totalRecordCount := models.GetDocumentTypesCountFromDb(db)
	documentTypes, err := models.GetPaginateDocumentTypesFromDb(db, domain.DbPaginator{Limit: int(limit), Offset: int((page - 1) * limit)})
	if err != nil {
		log.Println("DocumentTypeListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	totalPage := math.Ceil(float64(totalRecordCount) / float64(limit))
	nextPage := int32(page + 1)
	if nextPage > int32(totalPage) {
		nextPage = 1
	}
	paginator := domain.Paginator{TotalRecord: int32(totalRecordCount), PageSize: int32(limit), CurrentPage: int32(page), NextPage: nextPage, TotalPage: int32(totalPage)}
	responseJson, _ := json.Marshal(models.DocumentTypeResponse{Paginator: paginator, Data: documentTypes})
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	response.Write(responseJson)
}

func DocumentTypeDetailView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	idAsUuid, err := uuid.Parse(vars["id"])
	if err != nil {
		log.Println("DocumentTypeDetailView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	documentType, err := models.GetDocumentTypeInDbByID(db, domain.DocumentTypeDto{Id: idAsUuid})
	documentTypeJson, _ := json.Marshal(documentType)
	response.WriteHeader(http.StatusOK)
	response.Write(documentTypeJson)
}
