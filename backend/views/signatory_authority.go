package views

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
	"net/http"
)

func SignatoryAuthorityListView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	signatoryAuthorities, err := models.GetSignatoryAuthoritiesFromDb(db)
	if err != nil {
		log.Println("SignatoryAuthorityListView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	signatoryAuthoritiesJson, _ := json.Marshal(signatoryAuthorities)
	response.WriteHeader(http.StatusOK)
	response.Write(signatoryAuthoritiesJson)
}

func SignatoryAuthorityDetailView(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	db := pkg.GetDatabaseConnection()
	defer db.Close()
	vars := mux.Vars(request)
	idAsUuid, err := uuid.Parse(vars["id"])
	if err != nil {
		log.Println("SignatoryAuthorityDetailView: ", err)
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	signatoryAuthority, err := models.GetSignatoryAuthorityInDbByID(db, domain.SignatoryAuthorityDto{Id: idAsUuid})
	signatoryAuthorityJson, _ := json.Marshal(signatoryAuthority)
	response.WriteHeader(http.StatusOK)
	response.Write(signatoryAuthorityJson)
}
