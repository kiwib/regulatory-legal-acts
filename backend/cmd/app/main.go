package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/kiwib/regulatory-legal-acts/views"
	"log"
	"net/http"
	"time"
)

func getRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/api/sections/", views.SectionListView)
	router.HandleFunc("/api/sections/{code}/", views.SectionDetailView)
	router.HandleFunc("/api/sections/{code}/subsections/", views.SectionDetailSubSectionListView)
	router.HandleFunc("/api/sections/{code}/subsections/{subCode}/", views.SectionDetailSubSectionDetailView)

	router.HandleFunc("/api/regulatory-legal-acts/", views.RegulatoryLegalActListView)
	router.HandleFunc("/api/regulatory-legal-acts/{id}/", views.RegulatoryLegalActDetailView)

	router.HandleFunc("/api/document-types/", views.DocumentTypeListView)
	router.HandleFunc("/api/document-types/{id}/", views.DocumentTypeDetailView)

	router.HandleFunc("/api/signatory-authorities/", views.SignatoryAuthorityListView)
	router.HandleFunc("/api/signatory-authorities/{id}/", views.SignatoryAuthorityDetailView)
	return router
}

func main() {
	serverAddr := fmt.Sprintf("%s:%s", "0.0.0.0", "80")
	log.Println("Server started on address:", serverAddr)
	srv := &http.Server{
		Handler:      getRouter(),
		Addr:         serverAddr,
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  30 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
