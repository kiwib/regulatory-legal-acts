package main

import (
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
)

func main() {
	// функционал для выкачки и инициализации
	db := pkg.GetDatabaseConnection()
	sections, err := models.GetSectionsFromDb(db)
	if err != nil {
		log.Fatalln("Initialize subsections: ", err)
	}
	for _, section := range sections {
		subSection, err := pkg.GetSubSectionsForSection(section)
		if err != nil {
			log.Println("Initialize subsections| Not processed section: ", section, err)
		}
		_, err = models.InitializeSectionsIntoDb(db, subSection)
		if err != nil {
			log.Println("Initialize subsections| Not processed section: ", section, err)
		}
	}
}
