package main

import (
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
)

func main() {
	// функционал для выкачки и инициализации
	documentTypes, err := pkg.GetDocumentTypes()
	if err != nil {
		log.Fatalln("Initialize document types: ", err)
	}
	db := pkg.GetDatabaseConnection()
	_, err = models.InitializeDocumentTypesIntoDb(db, documentTypes)
	if err != nil {
		log.Fatalln("Initialize document types: ", err)
	}
}
