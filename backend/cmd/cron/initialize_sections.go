package main

import (
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
)

func main() {
	// функционал для выкачки и инициализации
	sections, err := pkg.GetSection()
	if err != nil {
		log.Fatalln("Initialize sections: ", err)
	}
	db := pkg.GetDatabaseConnection()
	_, err = models.InitializeSectionsIntoDb(db, sections)
	if err != nil {
		log.Fatalln("Initialize sections: ", err)
	}
}
