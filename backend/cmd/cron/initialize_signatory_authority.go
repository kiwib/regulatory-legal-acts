package main

import (
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
)

func main() {
	// функционал для выкачки и инициализации
	signatoryAuthorities, err := pkg.GetSignatoryAuthorities()
	if err != nil {
		log.Fatalln("Initialize signatory authorities: ", err)
	}
	db := pkg.GetDatabaseConnection()
	_, err = models.InitializeSignatoryAuthorityIntoDb(db, signatoryAuthorities)
	if err != nil {
		log.Fatalln("Initialize signatory authorities: ", err)
	}
}
