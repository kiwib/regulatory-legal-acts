package main

import (
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"gitlab.com/kiwib/regulatory-legal-acts/pkg"
	"log"
)

func main() {
	regulatoryLegalActs, err := pkg.GetRegulatorLegalActs()
	if err != nil {
		log.Fatalln("Initialize regulatory legal acts: ", err)
	}
	db := pkg.GetDatabaseConnection()
	_, err = models.InitializeRegulatoryLegalActsIntoDb(db, regulatoryLegalActs)
	if err != nil {
		log.Fatalln("Initialize regulatory legal acts: ", err)
	}
}
