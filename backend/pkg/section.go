package pkg

import (
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/models"
	"io/ioutil"
	"log"
	"net/http"
)

func GetSubSectionsForSection(section models.Section) ([]domain.SectionDto, error) {
	/* Метод для получения подблоков блока */
	url := fmt.Sprintf("http://publication.pravo.gov.ru/api/SubBlock/Get?code=%s", section.Code)
	resp, err := http.Get(url)
	if err != nil {
		log.Println("GetSubSectionsForSection: ", err)
		return []domain.SectionDto{}, err
	}
	// в конце выполнения закрываем
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("GetSubSectionsForSection: ", err)
		return []domain.SectionDto{}, err
	}
	var subSectionDtoList []domain.SectionDto
	err = json.Unmarshal(data, &subSectionDtoList)
	if err != nil {
		log.Fatalln("GetSubSectionsForSection: ", err)
		return []domain.SectionDto{}, err
	}
	log.Printf("GetSubSectionsForSection: Was received %d subsection for section with code %s", len(subSectionDtoList), section.Code)
	for index, _ := range subSectionDtoList {
		subSectionDtoList[index].ParentCode = section.Code
	}
	return subSectionDtoList, nil
}

func GetSection() ([]domain.SectionDto, error) {
	/* Функция для получения списка блоков (разделов) */
	resp, err := http.Get("http://publication.pravo.gov.ru/api/PublicBlock/Get")
	if err != nil {
		log.Println(err)
		return []domain.SectionDto{}, err
	}
	// в конце выполнения закрываем
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return []domain.SectionDto{}, err
	}
	var sectionDtoList []domain.SectionDto
	err = json.Unmarshal(data, &sectionDtoList)
	if err != nil {
		log.Fatalln(err)
		return []domain.SectionDto{}, err
	}
	log.Printf("Was received %d section", len(sectionDtoList))
	return sectionDtoList, nil
}

func InitializeSectionsIntoDb(db *sqlx.DB, sections []domain.SectionDto) (bool, error) {
	/* Метод для инициализации блоков (разделов) в БД */
	processedCounter := 0
	for _, section := range sections {
		_, err := models.UpdateOrCreateSectionIntoDb(db, section)
		if err != nil {
			log.Println("Not processed object: ", section)
			continue
		}
		processedCounter += 1
	}
	log.Println(fmt.Sprintf("Was proccessed %d records", processedCounter))
	return false, nil
}

func InitializeSubSectionForSection(db *sqlx.DB, section models.Section) (bool, error) {
	/* Метод для инициализации подблоков для блока */
	subSections, err := GetSubSectionsForSection(section)
	if err != nil {
		log.Println("InitializeSubSectionForSection: ", err)
		return false, nil
	}
	_, err = InitializeSectionsIntoDb(db, subSections)
	if err != nil {
		log.Println("InitializeSubSectionForSection: ", err)
		return false, err
	}
	return true, nil
}

func InitializeSubSectionForSections(db *sqlx.DB) (bool, error) {
	/* Метод для инициализации подблоков для блоков */
	sections, err := models.GetSectionsFromDb(db)
	if err != nil {
		log.Println("InitializeSubSectionForSection: ", err)
		return false, err
	}
	for _, section := range sections {
		if _, err := InitializeSubSectionForSection(db, section); err != nil {
			log.Println(fmt.Sprintf("CONTINUE| Section #%d| InitializeSubSectionForSection: ", err))
			continue
		}
	}
	return true, nil
}
