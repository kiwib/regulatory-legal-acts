package pkg

import (
	"encoding/json"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"io/ioutil"
	"log"
	"net/http"
)

func GetDocumentTypes() ([]domain.DocumentTypeDto, error) {
	/* Функция для получения списка видов документа */
	resp, err := http.Get("http://publication.pravo.gov.ru/api/DocumentType/Get")
	if err != nil {
		log.Println(err)
		return []domain.DocumentTypeDto{}, err
	}
	// в конце выполнения закрываем
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return []domain.DocumentTypeDto{}, err
	}
	var documentTypeDtoList []domain.DocumentTypeDto
	err = json.Unmarshal(data, &documentTypeDtoList)
	if err != nil {
		log.Fatalln(err)
		return []domain.DocumentTypeDto{}, err
	}
	log.Printf("Was received %d document types", len(documentTypeDtoList))
	return documentTypeDtoList, nil
}
