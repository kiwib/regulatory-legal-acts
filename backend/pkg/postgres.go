package pkg

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"gitlab.com/kiwib/regulatory-legal-acts/utils"
	"log"
)

func GetPostgresDB(config domain.DataBaseConnectionConfig) (*sqlx.DB, error) {
	connectionString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", config.Host, config.Port, config.Username, config.DBName, config.Password, config.SSLMode)
	db, err := sqlx.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func GetDatabaseConnection() *sqlx.DB {
	db, err := GetPostgresDB(domain.DataBaseConnectionConfig{
		SSLMode:  utils.GetEnvOrDefault("SSLMODE", "disable"),
		Port:     utils.GetEnvOrDefault("DATABASE_PORT", "5432"),
		Host:     utils.GetEnvOrDefault("DATABASE_HOST", "localhost"),
		Password: utils.GetEnvOrDefault("DATABASE_PASSWORD", "12345"),
		DBName:   utils.GetEnvOrDefault("DATABASE_NAME", "regulatory_legal_acts"),
		Username: utils.GetEnvOrDefault("DATABASE_USERNAME", "postgres"),
	})
	if err != nil {
		log.Fatalln("Database connection error", err)
	}
	return db
}
