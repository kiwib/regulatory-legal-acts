package pkg

import (
	"encoding/json"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"io/ioutil"
	"log"
	"net/http"
)

func GetSignatoryAuthorities() ([]domain.SignatoryAuthorityDto, error) {
	/* Функция для получения списка принявших органов */
	resp, err := http.Get("http://publication.pravo.gov.ru/api/SignatoryAuthority/Get")
	if err != nil {
		log.Println(err)
		return []domain.SignatoryAuthorityDto{}, err
	}
	// в конце выполнения закрываем
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return []domain.SignatoryAuthorityDto{}, err
	}
	var signatoryAuthorityDtoList []domain.SignatoryAuthorityDto
	err = json.Unmarshal(data, &signatoryAuthorityDtoList)
	if err != nil {
		log.Fatalln(err)
		return []domain.SignatoryAuthorityDto{}, err
	}
	log.Printf("Was received %d signatory authority", len(signatoryAuthorityDtoList))
	return signatoryAuthorityDtoList, nil
}
