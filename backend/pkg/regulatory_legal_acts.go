package pkg

import (
	"encoding/json"
	"fmt"
	"gitlab.com/kiwib/regulatory-legal-acts/domain"
	"io/ioutil"
	"log"
	"net/http"
)

func GetPageRegulatorLegalActs(pageNumber int32, rangeSize int32) (domain.RegularLegalActDocumentDto, error) {
	/* Метод для получения N-й страницы списка НПА  */
	url := fmt.Sprintf("http://publication.pravo.gov.ru/api/Document/Get?CurrentPageNumber=%d&rangeSize=%d", pageNumber, rangeSize)
	log.Printf("Url: %s; PageNumber: %d", url, pageNumber)
	resp, err := http.Get(url)
	if err != nil {
		log.Println("GetPageRegulatorLegalActs: ", err)
		return domain.RegularLegalActDocumentDto{}, err
	}
	// в конце выполнения закрываем
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("GetPageRegulatorLegalActs: ", err)
		return domain.RegularLegalActDocumentDto{}, err
	}
	var regularLegalActDocumentDto domain.RegularLegalActDocumentDto
	err = json.Unmarshal(data, &regularLegalActDocumentDto)
	if err != nil {
		log.Println(err)
		return domain.RegularLegalActDocumentDto{}, err
	}
	return regularLegalActDocumentDto, nil
}

func GetRegulatorLegalActs() ([]domain.RegularLegalActDto, error) {
	/* Метод для получения списка НПА */
	// получаем максимальное количество записей при обращении
	const rangeSize = 200
	// отправляем пробный запрос для определения общее количество доступных страниц
	regularLegalActDocumentDto, err := GetPageRegulatorLegalActs(1, rangeSize)
	if err != nil {
		log.Println("GetRegulatorLegalActs: ", err)
		return []domain.RegularLegalActDto{}, err
	}
	// TODO: Использовать горутины
	acc := make([]domain.RegularLegalActDto, 0, regularLegalActDocumentDto.TotalDocumentsCount)
	// TODO При разработке обрабатываем маленькое количество записей
	//for i := int32(2); i < regularLegalActDocumentDto.MaxPageNumber; i++ {
	for i := int32(66); i < 1000; i++ {
		data, err := GetPageRegulatorLegalActs(i, rangeSize)
		acc = append(acc, data.Documents...)
		log.Printf("%d/%d", i, regularLegalActDocumentDto.MaxPageNumber)
		if err != nil {
			log.Println("GetRegulatorLegalActs Loop: ", i, err)
		}
	}
	return acc, nil
}
