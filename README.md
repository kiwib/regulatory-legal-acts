# Прокси для [Нормативно-правовых актов](http://publication.pravo.gov.ru/)

## Адреса инстансов:

+ **[Релиз - Яндекс.Облако](https://regulator-legal-acts.shaykin-av.ru/)**

## Доступные точки доступа:

| Адрес                                             | Описание                                              |
| -------------                                     |:-------------:                                        |
| **/api/sections/**                                | Cписок блоков                                         |
| **/api/sections/{code}/**                         | Детальная информация о блоке                          |
| **/api/sections/{code}/subsections/**             | Список подблоков конкретного подблока                 |
| **/api/sections/{code}/subsections/{subCode}/**   | Детальная информация подблока                         |
| **/api/regulatory-legal-acts/**                   | Список НПА                                            |
| **/api/regulatory-legal-acts/{id}/**              | Детальная информация о НПА                            |
| **/api/document-types/**                          | Список видов документа                                |
| **/api/document-types/{id}/**                     | Детальная информация о виде документа                 |
| **/api/signatory-authorities/**                   | Список принимающих органов                            |
| **/api/signatory-authorities/{id}/**              | Детальная информация о принимающем органе             |
