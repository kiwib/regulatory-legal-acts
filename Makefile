.PHONY:
.SILENT:

build:
	go build -o ./.bin/app ./cmd/app/main.go

build-app-alpine:
	echo "Start build app ..."
	GOOS=linux GOARCH=amd64 CXX=g++ CC=gcc CGO_ENABLED=0 \
	go build -o ./.bin/app ./cmd/app/main.go
	echo "Success build app"

build-initialize-regulatory-legal-acts:
	echo "Start build initialize regulatory legal acts..."
	GOOS=linux GOARCH=amd64 CXX=g++ CC=gcc CGO_ENABLED=0 \
	go build -o ./.bin/cron/initialize_regulatory_legal_acts ./cmd/cron/initialize_regulatory_legal_acts.go
	echo "Success built initialize regulatory legal acts..."


build-initialize-sections:
	echo "Start build initialize sections acts..."
	GOOS=linux GOARCH=amd64 CXX=g++ CC=gcc CGO_ENABLED=0 \
	go build -o ./.bin/cron/initialize_sections ./cmd/cron/initialize_sections.go
	echo "Success built initialize sections acts..."

build-initialize-subsections:
	echo "Start build initialize subsections acts..."
	GOOS=linux GOARCH=amd64 CXX=g++ CC=gcc CGO_ENABLED=0 \
	go build -o ./.bin/cron/initiliaze_subsections ./cmd/cron/initiliaze_subsections.go
	echo "Success built initialize subsections acts..."

build-initialize-document-types:
	echo "Start build initialize document types acts..."
	GOOS=linux GOARCH=amd64 CXX=g++ CC=gcc CGO_ENABLED=0 \
	go build -o ./.bin/cron/initilize_document_types ./cmd/cron/initilize_document_types.go
	echo "Success built initialize document types acts..."

run: build
	./.bin/app

migrate-db-up:
	migrate version
	#migrate -path ./migrations -database 'postgres://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}?sslmode=disable' up

migrate-db-down:
	migrate -path ./migrations -database 'postgres://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}?sslmode=disable' down

migrate-local-db-up:
	migrate -path ./migrations -database 'postgres://postgres:12345@localhost:5432/regulatory_legal_acts?sslmode=disable' up

migrate-local-db-down:
	migrate -path ./migrations -database 'postgres://postgres:12345@localhost:5432/regulatory_legal_acts?sslmode=disable' down

