import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Block from '../views/Block'
import DetailBlock from '../views/DetailBlock'
import Apparatus from '../views/Apparatus'
import Document from '../views/Document'
import Npa from '../views/Npa'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/Apparatus',
    name: 'Apparatus',
    component: Apparatus,
  },
  {
    path: '/Block',
    name: 'Block',
    component: Block,
  },
  {
    path: '/Block/:code',
    name: 'DetailBlock',
    component: DetailBlock,
  },
  {
    path: '/Document',
    name: 'Document',
    component: Document,
  },
  {
    path: '/Npa',
    name: 'Npa',
    component: Npa,
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

export default router
